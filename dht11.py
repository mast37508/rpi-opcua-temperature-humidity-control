import time
import board
import adafruit_dht
import psutil
# We first check if a libgpiod process is running. If yes, we kill it!
for proc in psutil.process_iter():
    if proc.name() == 'libgpiod_pulsein' or proc.name() == 'libgpiod_pulsei':
        proc.kill()
sensor = adafruit_dht.DHT11(board.D24)

def get_temp():
    temp = sensor.temperature
    print("Temperature: {}*C".format(temp))
    return temp
        
def get_hum():
    humidity = sensor.humidity
    print("Humidity: {}% ".format(humidity))
    return humidity

