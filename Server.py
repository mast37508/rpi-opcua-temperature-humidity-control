from opcua import Server
from dht11 import *
from led import *
import datetime
import time

server = Server()
#IP+Port
url = "opc.tcp://192.168.8.107:4841"
server.set_endpoint(url)

name = "OPCUA_SIMULATION_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()

Param = node.add_object(addspace, "Parameters")

Temp = Param.add_variable(addspace, "Temperature", 0)
Hum = Param.add_variable(addspace, "Humidity", 0)
Time = Param.add_variable(addspace, "Time", 0)

Temp.set_writable()
Hum.set_writable()
Time.set_writable()

server.start()
print("Server started at {}".format(url))

while True:
    try:
        Temperature = get_temp()
        Humidity = get_hum()
        TIME = datetime.datetime.now()
        print(Temperature, Humidity, TIME)
        print("----------------------------------------")
    except:
        continue
    if Humidity > 80:
        red()
    else:
        green()
    Temp.set_value(Temperature)
    Hum.set_value(Humidity)
    Time.set_value(TIME)
    time.sleep(2)

