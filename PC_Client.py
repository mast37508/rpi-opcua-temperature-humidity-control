from opcua import Client
import time

#Server IP+port
url = "opc.tcp://192.168.1.128:4841"

client = Client(url)
client.connect()
print("Client connected!")

while True:
    print("----------------------")
    Temp = client.get_node("ns=2;i=2")
    Temperature = Temp.get_value()
    print("Temperature: {} ".format(Temperature))
    Hum = client.get_node("ns=2;i=3")
    Humidity = Hum.get_value()
    print("Humidity: {} ".format(Humidity))
    TIME = client.get_node("ns=2;i=4")
    TIME_value = TIME.get_value()
    print("Timestamp: {} ".format(TIME_value))
    time.sleep(2)