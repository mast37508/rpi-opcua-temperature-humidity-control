import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
from time import sleep # Import the sleep function from the time module
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BCM)
GPIO.setup(20, GPIO.OUT, initial=GPIO.LOW) # Set pin 8 to be an output pin and set initial value to low (off)
GPIO.setup(21, GPIO.OUT, initial=GPIO.LOW) # Set pin 8 to be an output pin and set initial value to low (off)


def green():
    GPIO.output(20, GPIO.HIGH) # Turn on Green
    GPIO.output(21, GPIO.LOW) # Turn off Red
 
def red():
 GPIO.output(21, GPIO.HIGH) # Turn on Red
 GPIO.output(20, GPIO.LOW) # Turn off Green

