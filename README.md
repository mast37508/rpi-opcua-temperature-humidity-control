# RPi OPCUA Temperature Humidity control



## Getting started

0.) Connect LEDs, dht11 to RPi
dht11 pin: D24
Green LED pin: 20 
Red LED pin: 21

1.) Install all from README.TXT on RPi

2.) Move files onto RPi
Server.py led.py dht11.py

3.) Edit Server.py (IP address)

4.) Move file onto computer
PC_Client.py

5.) Edit PC_Client.py (IP address)

6.) Start Server.py on RPi and PC_Client.py on pc